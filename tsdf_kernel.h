#ifndef CUDA_KERNEL_H
#define CUDA_KERNEL_H

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "tsdf_config.h"

// CUDA worker performing TSDF integration on GPU device
__global__
void TSDFworker_k(
	float *depth_img_data, int img_width, int img_height,
	float *tsdf_grid_data, 
	float *weight_grid_data, 
	Eigen::Vector3d cam_pos,
	Eigen::Matrix3d cam_rot,
	cam_params cam_intrinsics
) {

	Eigen::Map<Eigen::MatrixXf> depth_img(
		depth_img_data, 
		img_height, img_width
	);
	Eigen::TensorMap<Eigen::Tensor<float, 3> > tsdf_grid(
		tsdf_grid_data,
		DIM_VOXEL_GRID,	DIM_VOXEL_GRID,	DIM_VOXEL_GRID
	);
	Eigen::TensorMap<Eigen::Tensor<float, 3> > weight_grid(
		weight_grid_data,
		DIM_VOXEL_GRID,	DIM_VOXEL_GRID,	DIM_VOXEL_GRID
	);

	// Iterate over voxel corner grid
	int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
	int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
	int idx_z = threadIdx.z + blockIdx.z * blockDim.z;

	// Convert each voxel corner from world coordinates to camera coordinates
	Eigen::Vector3d p(idx_x, idx_y, idx_z);

	p(0) *= VOXEL_SIZE;
	p(1) *= VOXEL_SIZE;
	p(2) *= VOXEL_SIZE;
	p(0) += GRID_OFFS_X;
	p(1) += GRID_OFFS_Y;
	p(2) += GRID_OFFS_Z;

	p -= cam_pos;
	p = cam_rot * p;

	// Discard points that are now "behind" camera
	if(p(2) > 0){

		// Get corresponding depth image coordinates for current voxel corner
		int img_idx_x = roundf(
			cam_intrinsics.fx * (p(0) / p(2)) + cam_intrinsics.cx
		);
		int img_idx_y = roundf(
			cam_intrinsics.fy * (p(1) / p(2)) + cam_intrinsics.cy
		);

		// Discard points that are outside of image coordinate borders
		if(
			img_idx_x >= 0 && 
			img_idx_x < img_width && 
			img_idx_y >= 0 && 
			img_idx_y < img_height
		) {

			// Obtain depth value & check for validity
			double depth = depth_img(img_idx_y, img_idx_x);
			if(depth > 0) {

				// Obtain signed distance, truncate it & scale between 0 and 1
				double dist = depth - p(2);

				dist /= TRUNC_LIM;

				if(abs(dist) <= 1) {

					// Fill TSDF value into voxel corner grid
					float weight_old = weight_grid(idx_x, idx_y, idx_z);
					weight_grid(idx_x, idx_y, idx_z) += 1;
					tsdf_grid(idx_x, idx_y, idx_z) = 
						(tsdf_grid(idx_x, idx_y, idx_z) * weight_old + dist) 
						/ 
						weight_grid(idx_x, idx_y, idx_z);

				}
			}
		}
	}
}

#endif