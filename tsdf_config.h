#ifndef TSDF_CONFIG_H
#define TSDF_CONFIG_H

#define DIM_VOXEL_GRID 500            // dimension of cubic voxel grid. Choose multiple of 10 for thread allocation
#define VOXEL_SIZE     0.005          // [m]
#define TRUNC_LIM      VOXEL_SIZE * 5
#define GRID_OFFS_X   -2.5            // [m]
#define GRID_OFFS_Y   -1.5            // [m]
#define GRID_OFFS_Z    1.5            // [m]

struct cam_params {
	float cx;
	float cy;
	float fx;
	float fy;
};

#endif