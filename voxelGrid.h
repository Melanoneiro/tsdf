#ifndef VOXELGRID_H
#define VOXELGRID_H

#include <vector>
#include <algorithm>
#include <chrono>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

//#include "marchingCubes_kernel.h"
#include "table_triangles.h"

using namespace std;

// TODO: split this up into multiple files!

// Forward declaration for the kernels

template<class T>
__global__
void setConstant_k(T *data, const T val);

template<class T>
__global__
void numByLim_k(
	T *voxel_grid_data, 
	T lim, 
	int dim, 
	uint32_t *num
);
template<class T>
__global__
void extractByLim_k(
	T *voxel_grid_data, int *v_sparse_data,
	uint32_t num_points, T lim, int dim, int *p_cntr
);

__global__
void getTriangleNum_k(
	float *tsdf_grid_data,
	float dim,
	uint32_t *tri_cntr
);
__device__
Eigen::Vector3f zeroCross(
	Eigen::Vector3i p1_i, Eigen::Vector3i p2_i,
	float f1, float f2
);
__global__
void marchingCubesWorker_k(
	float *tsdf_grid_data, float *tri_data,
	int dim, int num_triangles, uint32_t *tri_cntr
);

template<class T>
class voxelGridGPU : public Eigen::TensorMap<Eigen::Tensor<T, 3> > {
public:

	__host__
	voxelGridGPU(int dim)
	: dim_(dim),
		is_map_(false),
		// This construct first allocates CUDA memory and then inits the base class:
		Eigen::TensorMap<Eigen::Tensor<T, 3> >(
			(cudaMallocManaged(&data_, dim * dim * dim * sizeof(T)), data_), 
			dim, dim, dim
		)
	{

		if((dim % 10) != 0 || dim < 0){
			printf("voxelGridGPU: Dimension must be a positive multiple of 10!\n");
			delete this;
		}

	}

	__host__ __device__
	voxelGridGPU(T *data, int dim)
	: dim_(dim),
		is_map_(true),
		data_(data),
		Eigen::TensorMap<Eigen::Tensor<T, 3> >(
			data,
			dim, dim, dim
		)
	{

		if((dim % 10) != 0 || dim < 0){
			printf("voxelGridGPU: Dimension must be a positive multiple of 10!\n");
			delete this;
		}
	
	}

	__host__ __device__
	~voxelGridGPU() {
		#ifndef __CUDA_ARCH__
		if(!this->is_map_)
			cudaFree(data_);
		#endif
	}

	using Eigen::TensorMap<Eigen::Tensor<T, 3> >::operator();
	__host__ __device__
	T operator()(Eigen::Vector3i idx) {
		return (*this)(idx(0), idx(1), idx(2));
	}

	// Override non-virtual base class function
	voxelGridGPU* setConstant(const T val) {

		setConstant_k<T> <<< (dim_ * dim_ * dim_) / 1000, 1000 >>> (data_, val);
		cudaDeviceSynchronize();

		return this;
	}

	// Override non-virtual base class function
	voxelGridGPU* setZero() {
		return setConstant(0);
	}

	vector<Eigen::Vector3d> toSparse(T lim) {

		// Measure runtime
		auto ti_start = chrono::high_resolution_clock::now();
		
		cout << endl << "Converting grid to sparse: ";

		// Cuda iterators
		dim3 block(10, 10, 10);
		dim3 grid(dim_/10, dim_/10, dim_/10);


		// Obtain number of points
		uint32_t *num_points;
		cudaMallocManaged(&num_points, sizeof(uint32_t));
		*num_points = 0;

		numByLim_k<T> <<< grid, block >>> (data_, lim, dim_, num_points);
		cudaDeviceSynchronize();

		cout << "found " << *num_points << " points meeting conditions." << endl;
		cout << "Extracting pointcloud... ";


		// Create sparse vector on GPU
		int *v_sparse_data;
		cudaMallocManaged(&v_sparse_data, 3 * (*num_points) * sizeof(int));

		int *p_cntr;
		cudaMallocManaged(&p_cntr, sizeof(int));
		*p_cntr = 0;

		extractByLim_k<T> <<< grid, block >>> (
			data_, v_sparse_data, *num_points, lim, dim_, p_cntr
		);
		cudaDeviceSynchronize();


		// Create vector from raw data
		vector<Eigen::Vector3d> v_sparse;
		for(int i = 0; i < (3 * (*num_points)); i+=3) {

			v_sparse.push_back(
				Eigen::Vector3d(
					v_sparse_data[i], 
					v_sparse_data[i+1], 
					v_sparse_data[i+2]
				)
			);

		}

		// Measure runtime
		auto ti_end = chrono::high_resolution_clock::now();
		chrono::duration<double> ti_elapsed = ti_end - ti_start;
		cout << "Done in " << ti_elapsed.count() << " seconds." << endl;

		cudaFree(num_points);
		cudaFree(v_sparse_data);
		cudaFree(p_cntr);
		
		return v_sparse;
	}

	bool writePoints(string dest, float lim = 0.5, float voxel_size = 1.0f) {

		cout << endl << "Exporting PLY to file: " + dest << endl;

		vector<Eigen::Vector3d> v_sparse = this->toSparse(lim);

		ofstream out_ply(dest, ofstream::out);
		if(!out_ply.is_open())
			return false;

		out_ply
			<< "ply" << endl
			<< "format ascii 1.0" << endl
			<< "element vertex " << v_sparse.size() << endl
			<< "property float x" << endl
			<< "property float y" << endl
			<< "property float z" << endl
			<< "end_header" << endl;

		for(auto p : v_sparse) {
			p *= voxel_size;
			out_ply << p(0) << " " << p(1) << " " << p(2) << endl;
		}

		out_ply.close();

		cout << "PLY exported successfully." << endl;

		return true;
	}

	bool writeMesh(string dest, float voxel_size = 1.0f) {

		// Measure runtime
		auto ti_start = chrono::high_resolution_clock::now();

		cout << "Running Marching Cubes to generate mesh... ";

		dim3 block(10, 10, 10);
		dim3 grid(dim_/10, dim_/10, dim_/10);

		// Obtain number of triangles
		uint32_t *tri_cntr;
		cudaMallocManaged(&tri_cntr, sizeof(uint32_t));
		*tri_cntr = 0;

		getTriangleNum_k <<< grid, block >>> (
			data_, dim_, tri_cntr
		);
		cudaDeviceSynchronize();

		int num_triangles = *tri_cntr;

		// Allocate memory for triangles and calculate them
		*tri_cntr = 0;
		float *tri_data;
		cudaMallocManaged(&tri_data, 9 * num_triangles * sizeof(float));
		marchingCubesWorker_k <<< grid, block >>> (
			data_, tri_data, 
			dim_, num_triangles, tri_cntr
		);
		cudaDeviceSynchronize();

		// Measure runtime
		auto ti_end = chrono::high_resolution_clock::now();
		chrono::duration<double> ti_elapsed = ti_end - ti_start;
		cout << "Done in " << ti_elapsed.count() << " seconds." << endl;

		cout << "Obtained " << num_triangles << " triangles from pointcloud." << endl;
		cout << "Writing mesh to file: " << dest << " ... ";

		ofstream out_mesh(dest, ofstream::out);
		if(!out_mesh.is_open())
			return false;

		Eigen::Map<Eigen::Matrix<float, 3, Eigen::Dynamic> > triangles(
			tri_data,
			3, 3 * num_triangles
		);

		out_mesh
			<< "ply" << endl
			<< "format ascii 1.0" << endl
			<< "element vertex " << (3 * num_triangles) << endl
			<< "property float x" << endl
			<< "property float y" << endl
			<< "property float z" << endl
			<< "element face " << num_triangles << endl
			<< "property list uchar int vertex_indices" << endl
			<< "end_header" << endl;

		for(int i = 0; i < (3 * num_triangles); i++) {
			Eigen::Vector3f p = triangles.block<3, 1>(0, i);
			p *= voxel_size;
			out_mesh << p(0) << " " << p(1) << " " << p(2) << endl;
		}

		for(int i = 0; i < (3 * num_triangles); i+=3) {
			out_mesh << "3 " << i << " " << i + 1 << " " << i + 2 << " " << endl;
		}

		out_mesh.close();

		cout << "Done. Mesh exported successfully." << endl;

		return true;

	}

private:
	T *data_;
	int dim_;
	bool is_map_;

};

// TODO: split this up into multiple files!

template<class T>
__global__
void setConstant_k(T *data, const T val) {

  int i = blockIdx.x * blockDim.x + threadIdx.x;

  data[i] = val;

  return;
}

// obtain number of points below lim
template<class T>
__global__
void numByLim_k(
	T *voxel_grid_data, 
	T lim, 
	int dim, 
	uint32_t *num
) {

	voxelGridGPU<T> voxel_grid(voxel_grid_data, dim);

	int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
	int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
	int idx_z = threadIdx.z + blockIdx.z * blockDim.z;

	if(abs(voxel_grid(idx_x, idx_y, idx_z)) <= lim)
		atomicAdd(num, 1);

	return;
}

template<class T>
__global__
void extractByLim_k(
	T *voxel_grid_data, int *v_sparse_data,
	uint32_t num_points, T lim, int dim, int *p_cntr
) {

	voxelGridGPU<T> voxel_grid(voxel_grid_data, dim);

	Eigen::Map<Eigen::Matrix<int, 3, Eigen::Dynamic> > v_sparse(
		v_sparse_data, 
		3, num_points
	);

	int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
	int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
	int idx_z = threadIdx.z + blockIdx.z * blockDim.z;

	// Add point to preassigned memory block at unique index
	if(abs(voxel_grid(idx_x, idx_y, idx_z)) <= lim) {

		int idx = atomicAdd(p_cntr, 1);

		v_sparse(0, idx) = idx_x;
		v_sparse(1, idx) = idx_y;
		v_sparse(2, idx) = idx_z;

	}

	return;
}

// TODO: split this up into multiple files!

// Indices of vertices and edges on one voxel.
// Corners are vertices(v), borders are edges(e).
//
//                           e6
//                   v7---------------v6
//                   /|              /|
//                e7/ |           e5/ |
//                 /  |e11         /  |e10
//                /   |   e4      /   |
//              v4---------------v5   |
//               |    |          |    |
//               |   v3----------|----v2
//             e8|   /     e2    |   /
//  Z  Y         |  /e3        e9|  /e1
//  | /          | /             | /
//  |/           |/       e0     |/
//  +--- X      v0---------------v1

// Calculate number of triangles obtained by marching cube algorithm
__global__
void getTriangleNum_k(
	float *tsdf_grid_data,
	float dim,
	uint32_t *tri_cntr
) {

	voxelGridGPU<float> tsdf_grid(tsdf_grid_data, dim);

	// Iterate over voxel corner grid
	int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
	int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
	int idx_z = threadIdx.z + blockIdx.z * blockDim.z;

	// leave out outermost voxel corners
	if((idx_x < (dim - 1)) && (idx_y < (dim - 1)) && (idx_z < (dim - 1))) {

		// indices of all vertices
		Eigen::Vector3i idx_v0(idx_x+0, idx_y+0, idx_z+0);
		Eigen::Vector3i idx_v1(idx_x+1, idx_y+0, idx_z+0);
		Eigen::Vector3i idx_v2(idx_x+1, idx_y+1, idx_z+0);
		Eigen::Vector3i idx_v3(idx_x+0, idx_y+1, idx_z+0);
		Eigen::Vector3i idx_v4(idx_x+0, idx_y+0, idx_z+1);
		Eigen::Vector3i idx_v5(idx_x+1, idx_y+0, idx_z+1);
		Eigen::Vector3i idx_v6(idx_x+1, idx_y+1, idx_z+1);
		Eigen::Vector3i idx_v7(idx_x+0, idx_y+1, idx_z+1);

		// Obtain indices of all vertices inside of object (tsdf < 0).
		// This yields a unique 8-bit-index for this cube

		// For each vertex, check if its value is ínside of object
		uint8_t vertcs_in_obj = (
			((tsdf_grid(idx_v0) < 0) << 0) |
			((tsdf_grid(idx_v1) < 0) << 1) |
			((tsdf_grid(idx_v2) < 0) << 2) |
			((tsdf_grid(idx_v3) < 0) << 3) |
			((tsdf_grid(idx_v4) < 0) << 4) |
			((tsdf_grid(idx_v5) < 0) << 5) |
			((tsdf_grid(idx_v6) < 0) << 6) |
			((tsdf_grid(idx_v7) < 0) << 7)
		);

		// cube doesn't have any init values
		bool cube_valid = (
			(tsdf_grid(idx_v0) <= 1) &
			(tsdf_grid(idx_v1) <= 1) &
			(tsdf_grid(idx_v2) <= 1) &
			(tsdf_grid(idx_v3) <= 1) &
			(tsdf_grid(idx_v4) <= 1) &
			(tsdf_grid(idx_v5) <= 1) &
			(tsdf_grid(idx_v6) <= 1) &
			(tsdf_grid(idx_v7) <= 1)
		);

		// TODO: also consider partial cubes?
		uint8_t idx_cube = vertcs_in_obj * cube_valid;

		// count triangles in this voxel and add them to global counter
		for(uint8_t i = 0; table_triangles[idx_cube][i] != -1; i+=3) {
			atomicAdd(tri_cntr, 1);
		}

	}

	return;
}

// Interpolate linearly between 2 vertices and return zero crossing point
__device__
Eigen::Vector3f zeroCross(
	Eigen::Vector3i p1_i, Eigen::Vector3i p2_i,
	float f1, float f2
) {

	Eigen::Vector3f p0(0,0,0);
	Eigen::Vector3f p1 = p1_i.cast<float>();
	Eigen::Vector3f p2 = p2_i.cast<float>();

	if((f1 <= 0) && (f2 >= 0)){
		return (p1 + ((p2 - p1) * (-f1 / (f2 - f1))));

	} else if((f2 <= 0) && (f1 >= 0)) {
		return (p2 + ((p1 - p2) * (-f2 / (f1 - f2))));

	} else if(f1 == f2) {
		return p1;

	} else
		return p0;

}

__global__
void marchingCubesWorker_k(
	float *tsdf_grid_data, float *tri_data,
	int dim, int num_triangles, uint32_t *tri_cntr
){

	voxelGridGPU<float> tsdf_grid(tsdf_grid_data, dim);

	Eigen::Map<Eigen::Matrix<float, 3, Eigen::Dynamic> > triangles(
		tri_data,
		3, 3 * num_triangles
	);

	// Iterate over voxel corner grid
	int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
	int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
	int idx_z = threadIdx.z + blockIdx.z * blockDim.z;

	// leave out outermost voxel corners
	if((idx_x < (dim - 1)) && (idx_y < (dim - 1)) && (idx_z < (dim - 1))) {

		// indices of all vertices
		Eigen::Vector3i idx_v0(idx_x+0, idx_y+0, idx_z+0);
		Eigen::Vector3i idx_v1(idx_x+1, idx_y+0, idx_z+0);
		Eigen::Vector3i idx_v2(idx_x+1, idx_y+1, idx_z+0);
		Eigen::Vector3i idx_v3(idx_x+0, idx_y+1, idx_z+0);
		Eigen::Vector3i idx_v4(idx_x+0, idx_y+0, idx_z+1);
		Eigen::Vector3i idx_v5(idx_x+1, idx_y+0, idx_z+1);
		Eigen::Vector3i idx_v6(idx_x+1, idx_y+1, idx_z+1);
		Eigen::Vector3i idx_v7(idx_x+0, idx_y+1, idx_z+1);

		// Obtain indices of all vertices inside of object (tsdf < 0).
		// This yields a unique 8-bit-index for this cube

		// For each vertex, check if its value is ínside of object
		uint8_t vertcs_in_obj = (
			((tsdf_grid(idx_v0) < 0) << 0) |
			((tsdf_grid(idx_v1) < 0) << 1) |
			((tsdf_grid(idx_v2) < 0) << 2) |
			((tsdf_grid(idx_v3) < 0) << 3) |
			((tsdf_grid(idx_v4) < 0) << 4) |
			((tsdf_grid(idx_v5) < 0) << 5) |
			((tsdf_grid(idx_v6) < 0) << 6) |
			((tsdf_grid(idx_v7) < 0) << 7)
		);

		// cube doesn't have any init values
		bool cube_valid = (
			(tsdf_grid(idx_v0) <= 1) &
			(tsdf_grid(idx_v1) <= 1) &
			(tsdf_grid(idx_v2) <= 1) &
			(tsdf_grid(idx_v3) <= 1) &
			(tsdf_grid(idx_v4) <= 1) &
			(tsdf_grid(idx_v5) <= 1) &
			(tsdf_grid(idx_v6) <= 1) &
			(tsdf_grid(idx_v7) <= 1)
		);

		// TODO: also consider partial cubes?
		uint8_t idx_cube = vertcs_in_obj * cube_valid;

		Eigen::Vector3f triangle_corners[12];

		// interpolate between vertices to find triangle corner points in voxel
		triangle_corners[0] = // e0
			zeroCross(idx_v0, idx_v1, tsdf_grid(idx_v0), tsdf_grid(idx_v1));
		triangle_corners[1] = // e1
			zeroCross(idx_v1, idx_v2, tsdf_grid(idx_v1), tsdf_grid(idx_v2));
		triangle_corners[2] = // e2
			zeroCross(idx_v2, idx_v3, tsdf_grid(idx_v2), tsdf_grid(idx_v3));
		triangle_corners[3] = // e3
			zeroCross(idx_v3, idx_v0, tsdf_grid(idx_v3), tsdf_grid(idx_v0));
		triangle_corners[4] = // e4
			zeroCross(idx_v4, idx_v5, tsdf_grid(idx_v4), tsdf_grid(idx_v5));
		triangle_corners[5] = // e5
			zeroCross(idx_v5, idx_v6, tsdf_grid(idx_v5), tsdf_grid(idx_v6));
		triangle_corners[6] = // e6
			zeroCross(idx_v6, idx_v7, tsdf_grid(idx_v6), tsdf_grid(idx_v7));
		triangle_corners[7] = // e7
			zeroCross(idx_v7, idx_v4, tsdf_grid(idx_v7), tsdf_grid(idx_v4));
		triangle_corners[8] = // e8
			zeroCross(idx_v0, idx_v4, tsdf_grid(idx_v0), tsdf_grid(idx_v4));
		triangle_corners[9] = // e9
			zeroCross(idx_v1, idx_v5, tsdf_grid(idx_v1), tsdf_grid(idx_v5));
		triangle_corners[10] = // e10
			zeroCross(idx_v2, idx_v6, tsdf_grid(idx_v2), tsdf_grid(idx_v6));
		triangle_corners[11] = // e11
			zeroCross(idx_v3, idx_v7, tsdf_grid(idx_v3), tsdf_grid(idx_v7));


		Eigen::Matrix3f tri;

		// obtain triangles from their corners
		for(uint8_t i = 0; table_triangles[idx_cube][i] != -1; i+=3) {

			int tri_idx = atomicAdd(tri_cntr, 3);

			int e0 = table_triangles[idx_cube][i  ];
			int e1 = table_triangles[idx_cube][i+1];
			int e2 = table_triangles[idx_cube][i+2];

			triangles.block<3, 1>(0, tri_idx  ) = triangle_corners[e0];
			triangles.block<3, 1>(0, tri_idx+1) = triangle_corners[e1];
			triangles.block<3, 1>(0, tri_idx+2) = triangle_corners[e2];

		}

	}

	return;
}

#endif