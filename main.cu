#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "tsdf_config.h"
#include "frame.h"
#include "voxelGrid.h"

using namespace std;

int main(void) {

	// Create 3D voxel grids for TSDF in CUDA memory
	cout << "Setting up voxel grids... " << flush;
	voxelGridGPU<float> tsdf_grid(DIM_VOXEL_GRID);
	tsdf_grid.setConstant(2);

	voxelGridGPU<float> weight_grid(DIM_VOXEL_GRID);
	weight_grid.setZero();
	cout << "done." << endl;

	// Define input data
	string data_dir("./data/rgbd-frames/frame-");
	int frames_num_from = 150;
	int frames_num_to = 199;

	// Obtain camera intrinsics
	Frame frame;
	if(!frame.readCamIntrinsics("data/camera-intrinsics.txt")) {
		cout << "Could not read cam intrinsics" << endl;
		return -1;
	}

	// Iterate over all input frames 
	for(int i = frames_num_from; i <= frames_num_to; i++) {

		// Measure runtime
		auto ti_start = chrono::high_resolution_clock::now();

		// Get name of current frame
		char num_f[7];
		snprintf(num_f, 7, "%06d", i);

		// read current frame
		cout << "Processing " + data_dir + string(num_f) + "... ";
		if(!frame.read(data_dir + string(num_f))) {
			cout << "Problem reading frame " + data_dir + string(num_f) << endl;
			return -1;
		}

		frame.createTSDF(tsdf_grid.data(), weight_grid.data());

		// Measure runtime
		auto ti_end = chrono::high_resolution_clock::now();
		chrono::duration<double> ti_elapsed = ti_end - ti_start;
		cout << "Done in " << ti_elapsed.count() << " seconds." << endl;

	}

	tsdf_grid.writePoints("tsdf.ply", 0.9999, VOXEL_SIZE);

	tsdf_grid.writeMesh("mesh.ply", VOXEL_SIZE);

	return 0;
}