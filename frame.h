#ifndef FRAME_H
#define FRAME_H

#include <iostream>
#include <fstream>
#include <string>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

#include "tsdf_config.h"
#include "tsdf_kernel.h"

using namespace std;

class Frame {
public:
	Frame() {
	}

	bool read(string filename_stub) {
		return
			readDepthImg_(filename_stub + ".depth.png") &&
			//readColorImg_(filename_stub + ".color.png") &&
			readPose_(filename_stub + ".pose.txt");
	}

	bool readCamIntrinsics(string filename) {
		Eigen::Matrix3f mat;

		ifstream file(filename);
		if(!file.is_open())
			return false;

		for(int i_r = 0; i_r < 3; i_r++)
			for(int i_c = 0; i_c < 3; i_c ++)
				file >> mat(i_r, i_c);

		file.close();

		cam_intrinsics_.cx = mat(0, 2);
		cam_intrinsics_.cy = mat(1, 2);
		cam_intrinsics_.fx = mat(0, 0);
		cam_intrinsics_.fy = mat(1, 1);

		return true;
	}

	bool createTSDF(
		float *tsdf_grid_data, 
		float *weight_grid_data
	) {

		// Move depth image to CUDA memory
		float *gpu_depth_img_data;
		cudaMallocManaged(
			&gpu_depth_img_data, 
			depth_img_.size() * sizeof(float)
		);
		cudaMemcpy(
			gpu_depth_img_data, 
			depth_img_.data(), 
			depth_img_.size() * sizeof(float), 
			cudaMemcpyHostToDevice
		);

		// Call CUDA worker
		// Maximum number of threads per block is 1024. Use 10^10^10 = 1000.
		dim3 block(10,10,10);
		dim3 grid(DIM_VOXEL_GRID/10,DIM_VOXEL_GRID/10,DIM_VOXEL_GRID/10);
		TSDFworker_k<<< grid, block >>> (
			gpu_depth_img_data, depth_img_.cols(), depth_img_.rows(),
			tsdf_grid_data, weight_grid_data,
			cam_pos_, cam_rot_,
			cam_intrinsics_
		);
		cudaDeviceSynchronize();

		// Free allocated device memory
		cudaFree(gpu_depth_img_data);

		return true;
	}

private:
	cam_params cam_intrinsics_;
	Eigen::MatrixXf depth_img_;
	Eigen::MatrixXf color_img_;
	Eigen::Matrix3d cam_rot_;
	Eigen::Vector3d cam_pos_;

	bool readDepthImg_(string filename) {
		cv::Mat cv_depth_img = cv::imread(filename, cv::IMREAD_UNCHANGED);
		if(cv_depth_img.empty())
			return false;

		cv2eigen(cv_depth_img, depth_img_);
		depth_img_ *= 0.001;

		return true;
	}

	bool readColorImg_(string filename) {
		cv::Mat cv_color_img = cv::imread(filename, cv::IMREAD_UNCHANGED);
		if(cv_color_img.empty())
			return false;

		cv2eigen(cv_color_img, color_img_);

		return true;
	}

	bool readPose_(string filename) {
		ifstream file(filename);
		if(!file.is_open())
			return false;

		Eigen::Matrix4d cam_pose;
		for(int i_r = 0; i_r < 4; i_r++)
			for(int i_c = 0; i_c < 4; i_c ++)
				file >> cam_pose(i_r, i_c);

		file.close();

		// Account for matrix format, needs to be postprocessed
		cam_rot_ = cam_pose.block<3,3>(0,0).transpose();
		cam_pos_ = cam_pose.block<3,1>(0,3);

		return true;
	}

};

#endif